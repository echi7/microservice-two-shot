import React, { useEffect, useState } from 'react';


function HatForm() {
    const [locations, setLocations] = useState([]);
    const [name, setName] = useState('');
    const [fabric, setFabric] = useState('');
    const [style, setStyle] = useState('');
    const [color, setColor] = useState('');
    const [picture, setPicture] = useState('');
    const [location, setLocation] = useState('');

    const fetchLocation = async () => {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data);
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchLocation()
    }, []);

    const NameChange = (e) => {
        const value = e.target.value;
        setName(value)
    }
    const FabricChange = (e) => {
        const value = e.target.value;
        setFabric(value)
    }
    const StyleChange = (e) => {
        const value = e.target.value;
        setStyle(value)
    }
    const ColorChange = (e) => {
        const value = e.target.value;
        setColor(value)
    }
    const PictureChange = (e) => {
        const value = e.target.value;
        setPicture(value)
    }
    const LocationChange = (e) => {
        const value = e.target.value;
        setLocation(value)
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.fabric = fabric;
        data.style = style;
        data.color = color;
        data.picture_url = picture;
        data.location = location;
        console.log(location);

        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);

            setName('');
            setFabric('');
            setStyle('');
            setColor('');
            setPicture('');
            setLocation('');

        }
    }

    return (
        <>
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Enter a new hat</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input value={name} onChange={NameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={fabric} onChange={FabricChange} placeholder="Fabric Type" required type="text" name="fabric" id="fabric" className="form-control" />
                <label htmlFor="fabric">Fabric Type</label>
              </div>
              <div className="form-floating mb-3">
                <input value={style} onChange={StyleChange} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
                <label htmlFor="style">Style</label>
              </div>
              <div className="form-floating mb-3">
                <input value={color} onChange={ColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input value={picture} onChange={PictureChange} placeholder="Picture Url" required type="text" name="picture" id="picture" className="form-control" />
                <label htmlFor="picture">Picture Url</label>
              </div>
              <div className="mb-3">
                <select value={location} onChange={LocationChange} required id="location" name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                    <option key={ location.href } value={ location.href } >
                        { location.closet_name }
                    </option>
                    );
                })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </>
    )
}

export default HatForm;
