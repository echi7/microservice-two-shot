import React, { useState, useEffect } from 'react';


function HatsList() {
    const [hats, setHats] = useState()

    const fetchHats = async () => {
        const response = await fetch('http://localhost:8090/api/hats/');

        if (response.ok) {
            const data = await response.json();
            setHats(data.hats);
            console.log(data)
        }
    }


    useEffect(() => {
        fetchHats();
    }, [])

    const onDelete = async (delHat) => {
        const deleteUrl = `http://localhost:8090/api/hats/${delHat}`;
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(deleteUrl, fetchConfig)
        if(response.ok) {
            fetchHats()
        }
    }


    if (hats === undefined) {
        return undefined
    }
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Fabric</th>
                    <th>Style</th>
                    <th>Color</th>
                    <th>Picture Url</th>
                </tr>
            </thead>
            <tbody>
            {hats.map(hat => {
                return (
                <tr key={hat.href}>
                    <td>{ hat.name }</td>
                    <td>{ hat.fabric }</td>
                    <td>{ hat.style }</td>
                    <td>{ hat.color }</td>
                    <td><img src={ hat.picture_url } height="100" width="100"/></td>
                    <td>
                        <button onClick={() => onDelete(hat.id)} className="btn btn-primary">Delete</button>
                    </td>
                </tr>
                );
            })}
            </tbody>
        </table>
      );
}

export default HatsList;
