import React, { useState, useEffect } from 'react';

function ShoeList() {
    const [shoes, setShoes] = useState()

    const fetchShoes = async () => {
        const response = await fetch('http://localhost:8080/api/shoes/');

        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes)
        }
    }

    useEffect(() => {
        fetchShoes();
    }, []);

    const handleDelete = async (shoeID) => {
        const url = `http://localhost:8080/api/shoes/${shoeID}`;
        const fetchConfig = {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            fetchShoes()
        }
    }

    if (shoes === undefined){
        return undefined
    }

    return (
        <>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model</th>
                    <th>Color</th>
                    <th>Picture Url</th>
                    <th>Option</th>

                </tr>
            </thead>
            <tbody>
                {shoes.map(shoe => {
                    return(
                        <tr key={shoe.id}>
                            <td>{shoe.manufacturer}</td>
                            <td>{shoe.model_name}</td>
                            <td>{shoe.color}</td>
                            <td>{shoe.picture_url}</td>
                            <td>
                                <button onClick={() => handleDelete(shoe.id)}>Delete</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </>
    )
}

export default ShoeList;
