import React, { useEffect, useState } from 'react';

function ShoeForm() {
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {

        }
        data.manufacturer = manufacturer;
        data.model_name = model;
        data.color = color;
        data.picture_url = pictureUrl;
        data.bins = bin;

        console.log(data)

        const url =  'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers:
            {'Content-Type': 'application/json'}
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);
            setManufacturer('');
            setModel('');
            setColor('');
            setPictureUrl('');
            setBin('');
        }
    }
    const[bins, setBins] = useState([]);
    const[bin, setBin] = useState('');
    const handleBinsChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }
    const[manufacturer, setManufacturer] = useState('');
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }
    const[model, setModel] = useState('');
    const handleModelChange = (event) => {
        const value = event.target.value;
        setModel(value);
    }
    const[color, setColor] = useState('');
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const[pictureUrl, setPictureUrl] = useState('');
    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/'
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json()
            setBins(data.bins);
            console.log(data.bins)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Add a new shoe</h1>
                <form onSubmit={handleSubmit} id="create-shoe-form">
                  <div className="mb-3">
                    <div className="form-floating mb-3">
                      <input onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" value={manufacturer} />
                      <label htmlFor="manufacturer">Manufacturer</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input onChange={handleModelChange} placeholder="Model" required type="text" name="model" id="model" className="form-control" value={model} />
                      <label htmlFor="model">Model</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={color} />
                      <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input onChange={handlePictureUrlChange} placeholder="Picture URL" required type="text" name="pictureUrl" id="pictureUrl" className="form-control" value={pictureUrl} />
                      <label htmlFor="pictureUrl">Picture URL</label>
                    </div>
                    <select value={bin} onChange={handleBinsChange} required name="bins" id="bins" className="form-select">
                      <option value="">Choose a bin</option>
                      {bins.map(bin =>{
                        return (<option key={bin.id} value={bin.id}>{bin.closet_name}</option>)
                        })}
                    </select>
                    <button className="btn btn-primary">Add</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </>
      );

}



export default ShoeForm;
