# Wardrobify

Team:

* Eric - Hats
* Albert - Shoes

## Design

## Shoes microservice

The shoe microservice handles the management of the shoes, creating new shoes, retrieving list of shoes, and deleting shoes. We have a foreign key in the shoe model that points to a specific wardrobe where the shoe is stored. For example, I added a BinVO field to the shoe model. There is also a dropdown selection field in the ShoeForm component which allows the user to choose the associated bin. When submitting the create a shoe form, the id is included along with the other shoes details in the data which will allow the microservice to create a shoe associated with the corresponding bin. In the shoelist component, we modified the table to display the information on each of the shoes. We also added a delete button for each shoe in the shoelist component. I created a handleDelete function which makes a delete request to the show microservice api, passing the shoes ID in the URL.

## Hats microservice

Allows user to add and delete hats and specify type of hat and assign them to specific locations. We used Django backend and React for front end to complete our work. I created a Hat model that contained fields for:
    name = models.CharField(max_length=200)
    fabric = models.CharField(max_length=200)
    style = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
    )

The location foreign key was used in the view functions to grab data from the wardrobe microservice through a poller that would poll information into my LocationVO every 60 seconds.

My LocationVO contained these fields:

    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)

The import_href is our main way to identify the id of each location throughout this project and the closet_name was used to help with readability for the user in the front end.
