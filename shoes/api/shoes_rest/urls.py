from django.urls import path
from .views import api_list_shoes, api_show_shoe

urlpatterns = [
    path('shoes/', api_list_shoes, name="api-shoes"),
    path('shoes/delete/<int:id>/', api_show_shoe, name="api-delete-shoe"),
    path('shoes/<int:id>/', api_show_shoe, name="api-get-shoe"),
]
