from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from .models import Shoe, BinVO
import json
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder

# Create your views here.
class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "id"]



class ShoeEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "id",
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }

    def get_extra_data(self, o):
        return {"bins": o.bins.closet_name}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bins=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            bin_href = content["bins"]
            bins = BinVO.objects.get(import_href=bin_href)
            content["bins"] = bins
            # binvo = BinVO.objects.get(id=content["bins"])
            # content["bins"] = binvo
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bins id"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(shoe, encoder=ShoeEncoder, safe=False)


@require_http_methods(["DELETE", "GET"])
def api_show_shoe(request, id):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoeEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        shoe = Shoe.objects.get(id=id)
        shoe.delete()
        return JsonResponse({"deleted": True})
